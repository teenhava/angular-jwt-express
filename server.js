import express from 'express';
import bodyParser from 'body-parser';
 
import jwt from 'jsonwebtoken';
import fs from "fs";
import config from './config';
import cors from 'cors';

const app=express();

//cors Cross Origin Resource Sharing

app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", req.get("origin"));
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });


 // Body Parser 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


import indexRouter from './routes/index';

//initialise RoutesFolder

app.use('/', indexRouter);

//dynamic Production Port

var port = process.env.PORT || 3000;
//var port =3000;
app.listen(port,()=> console.log("Express server listening on port:"+port));

 

module.exports = app;

// app.route('/api/validatejwt')
//     .post(validate);

//  app.route('/api/generatejwt')
//     .post(generate);

// export function validate(req, res) {
//  // check header or url parameters or post parameters for token
//         var token = req.headers['x-auth-token'];
//         console.log(token);
//         if(token){
//           //Decode the token
//           jwt.verify(token,"secret-Key",(err,decod)=>{
//             if(err){
//               res.status(403).json({
//                 message:"Wrong Token"
//               });
//             }
//             else{
//               //If decoded then call next() so that respective route is called.
//               req.decoded=decod;

//               console.log(decod);
//               res.json({
//                 success: true,
//                 message: decod.admin,
//                 "token-decoded": decod
//             });
              
//             }
//           });
//         }
//         else{
//           res.status(403).json({
//             message:"No Token"
//           });
   
//         }
// }


// export function generate(req, res) {
//     const payload = {
//         admin: "HarshaVardhan"
//     };
//     var token = jwt.sign(payload, "secret-Key", {
//         expiresIn: 1440 // expires in 24 hours
//     });

//     // return the information including token as JSON
//     res.json({
//         success: true,
//         message: 'Enjoy your token 24 hrs!',
//         token: token
//     });
// }