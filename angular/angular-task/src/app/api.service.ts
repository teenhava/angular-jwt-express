import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
 
import {ActivatedRoute, Router} from '@angular/router';

@Injectable()
export class ApiService {

 token:any;
 url= "https://angular-jwt-server.herokuapp.com/api/";
  

  constructor(private http: Http, private hc: HttpClient ) { 
    this.token = localStorage.getItem('auth-token');
    
  }
   
  askjwt(){
    return this.http.post(this.url+"generatejwt",this.jwt()).map((response:Response) => response.json());
  }

  sendjwt(authtoken) {
    return this.http.post(this.url+"validatejwt", this.jwt()).map((response: Response) => response.json());
  }

  private jwt() {
    // create X-Auth-Token header with jwt token
  
    if (this.token) {
      console.log(this.token);
      let headers = new Headers({ 'X-Auth-Token': this.token });
      console.log(headers);
      return new RequestOptions({headers: headers});
    }
  }




}
