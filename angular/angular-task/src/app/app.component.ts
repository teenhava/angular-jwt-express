import { Component } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse,HttpClient} from '@angular/common/http';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs';
import  'rxjs/add/operator/map';


import {ApiService} from './api.service';
 
 


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'loding';
  url= "https://angular-jwt-server.herokuapp.com/api/";
  messagered="Token Expired in Red";
  messagegreen="Cuurent TExt";

  constructor(  private apiservice: ApiService) {
    
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    var authtoken = localStorage.getItem('auth-token');
    console.log(authtoken);
    if (authtoken) {
     
     
      this.apiservice.sendjwt(authtoken).subscribe(
        res =>{
          console.log(res)
          if(res){
            if(!res.success || res.message==="tokenexpired"){
              this.messagered=res.message;
            }
            else{
              this.messagegreen=res.message+"\n\n  "+"Token="+authtoken;
            }


          }
        },
        err => console.log(err)
      );
    }else {
      console.log('no Auth Token Exists');
      this.apiservice.askjwt().subscribe(
        res =>{
          console.log(res)
          localStorage.setItem('auth-token',res.token);
          this.messagegreen="New Token :"+res.token;

        },
        err => console.log(err)
      );
      
    }
  

 
  }

  // private jwt() {
  //   // create X-Auth-Token header with jwt token
  //   let authtoken = JSON.parse(localStorage.getItem('auth-token'));
  //   if (authtoken && authtoken.token) {
  //     let headers = new Headers( {'X-Auth-Token': authtoken.token});
  //     return new RequestOptions({headers: headers});
  //   }
  // }

  // askjwt(){
  //   return this.http.post(this.url+"generatejwt",this.jwt()).map((response:Response) => response.json());
  // }

  // sendjwt(authtoken) {
  //   return this.http.post(this.url+"validatejwt",
  //     authtoken, this.jwt()).map((response: Response) => response.json());
  // }

}
