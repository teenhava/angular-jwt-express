import express from 'express';
var router = express.Router();
import jwt from 'jsonwebtoken';

import configsecret from '../config';

// Sample Route
router.get('/api',(req,res)=>{
    res.json({
        message:'welcome to Harsha sample api'
    });
});

// First Time asking JWT
router.post('/api/generatejwt',function(req,res){

    const payload = {
        message: "Everybody loves Joseph"
    };
    var token = jwt.sign(payload, configsecret.secret, {
        expiresIn: 1440 // expires in 24 hours
    });

    // return the information including token as JSON
    res.json({
        success: true,
        message: 'Enjoy your token 24 hrs!',
        token: token
    });

});

//Validation JWT  Route

router.post('/api/validatejwt',function(req,res){

    // check header or url parameters or post parameters for token
    console.log(req.body.headers['X-Auth-Token'][0]);
    
   
    var token = req.body.headers['X-Auth-Token'][0];
    //console.log(token);

    if(token){
      //Decode the token
      jwt.verify(token,configsecret.secret,(err,decod)=>{
        if(err){
            if(err.name=="TokenExpiredError"){
                res.status(200).json({
                  success:false,
                  message:"tokenexpired"
                   
                });
              }  else{
                res.status(200).json({
                    success:false,
                    message:"Token Problem"
              });
            }
            }else{
          //If decoded then call next() so that respective route is called.
          req.decoded=decod;

          console.log(decod);
          res.json({
            success: true,
            message: decod.message,
            "token-decoded": decod
        });
          
        }
      });
    }
    else{
      res.status(403).json({
        success:false,
        message:"No Token"
      });

    }
});

module.exports = router;